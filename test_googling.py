from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import pytest

@pytest.fixture
def driver():
    driver = webdriver.Chrome()
    driver.get('https://google.co.id')
    yield driver
    driver.quit()

def test_googling(driver):
    driver.find_element(By.NAME, 'q').send_keys('Sandi Suryadi' + Keys.ENTER)
    assert 'Sandi Suryadi' in driver.find_element(By.CSS_SELECTOR, 'h3').text
    assert 'Sandi Suryadi' in driver.title
    driver.quit()

def test_googling_gavi(driver):
    driver.find_element(By.NAME, 'q').send_keys('Argavi Koto' + Keys.ENTER)
    assert 'Argavi Koto' in driver.find_element(By.CSS_SELECTOR, 'h3').text
    assert 'Argavi Koto' in driver.title
    driver.quit()

